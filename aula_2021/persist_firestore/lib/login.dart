import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:persist_firestore/home.dart';
import 'package:persist_firestore/main.dart';

class LoginPage extends StatelessWidget {
  final GlobalKey<FormState> _form = GlobalKey<FormState>();
  final FirebaseAuth auth = FirebaseAuth.instance;
  late String email;
  late String senha;
  LoginPage({Key? key}) : super(key: key);

  _authenticate(context) {
    _form.currentState!.save();
    auth
        .signInWithEmailAndPassword(email: this.email, password: this.senha)
        .then((value) => Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => HomePage())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Text("Login aluno"),
      ),
      body: Container(
        padding: EdgeInsets.all(15),
        child: Form(
          key: _form,
          child: Column(
            children: [
              TextFormField(
                onSaved: (value) => email = value!,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                    hintText: "Informe o email",
                    helperText: "Não deixe em branco"),
              ),
              TextFormField(
                onSaved: (value) => senha = value!,
                obscureText: true,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    hintText: "Informe a senha",
                    labelText: "Numérica",
                    helperText: "Não deixe em branco"),
              ),
              TextButton(
                  onPressed: () => _authenticate(context),
                  child: Text("Logar")),
              Padding(padding: EdgeInsets.fromLTRB(0, 50, 0, 0)),
            ],
          ),
        ),
      ),
    );
  }
}
