import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'apiexample.dart';

SharedPreferences prefs;
main() async {
  WidgetsFlutterBinding.ensureInitialized();
  prefs = await SharedPreferences.getInstance();
  print('aguardou');
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print('build aqui');
    return MaterialApp(
      title: 'Planetas',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: APIExample(),
    );
  }
}
/*
class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _scafoldKey = GlobalKey<ScaffoldState>();
  String nome;
  int idade;

  @override
  initState() {
    super.initState();
    nome = (prefs.containsKey("nome")) ? prefs.getString("nome") : "";
    idade = (prefs.containsKey("idade")) ? prefs.getInt("idade") : 0;
    print(nome);
    print(idade);
  }

  _salvarFormulario() {
    if (this._formKey.currentState.validate()) {
      this._formKey.currentState.save();
      prefs.setString("nome", nome);
      prefs.setInt("idade", idade);
      _scafoldKey.currentState.showSnackBar(SnackBar(
        duration: Duration(seconds: 2),
        content: Text("Salvo com sucesso"),
      ));
      showDialog(
        context: _scafoldKey.currentContext,
        builder: (context) => AlertDialog(
          title: Text("Salvo com sucesso"),
          content: Text("Conteúdo Salvo"),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scafoldKey,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
          child: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text("Nome"),
                  Padding(padding: EdgeInsets.fromLTRB(10, 0, 10, 0)),
                  Expanded(
                    child: TextFormField(
                      initialValue: nome,
                      onSaved: (newValue) => this.nome = newValue,
                      validator: (nome) =>
                          (nome == "") ? "Favor informar" : null,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          hintText: "Digite seu nome",
                          border: const OutlineInputBorder()),
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text("Idade"),
                  Padding(padding: EdgeInsets.fromLTRB(10, 0, 10, 0)),
                  Expanded(
                    child: TextFormField(
                      initialValue: (idade == 0) ? "" : idade.toString(),
                      keyboardType: TextInputType.text,
                      onSaved: (novaIdade) => idade = int.parse(novaIdade),
                      validator: (novaIdade) =>
                          (novaIdade == "") ? "Favor informar" : null,
                    ),
                  )
                ],
              ),
              FlatButton(
                  onPressed: () => _salvarFormulario(), child: Text("Salvar"))
            ],
          ),
        ),
      )),
    );
  }
}
*/
