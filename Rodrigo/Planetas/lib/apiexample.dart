import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'dart:convert';

class APIExample extends StatefulWidget {
  @override
  _APIExampleState createState() => _APIExampleState();
}

class _APIExampleState extends State<APIExample> {
  var _formKey = GlobalKey<FormState>();
  int numero;
  Future _planeta;

  _search() {
    _formKey.currentState.save();
    setState(() {
      String url = "https://swapi.dev/api/";
      _planeta = http.get(url + "planets/$numero/");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Busca de planetas'),
      ),
      floatingActionButton: IconButton(
          icon: Icon(
            Icons.search,
            color: Colors.blue,
          ),
          onPressed: () => _search()),
      body: Container(
        margin: new EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                  keyboardType: TextInputType.number,
                  onSaved: (numeroToSearch) =>
                      numero = int.parse(numeroToSearch),
                  decoration: InputDecoration(
                      labelText: "Digite o número do planeta",
                      border: OutlineInputBorder(
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(10.0),
                        ),
                      ))),
              FutureBuilder(
                future: _planeta,
                builder: (context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                    case ConnectionState.waiting:
                      return Text("Aguardando resposta");
                    default:
                      Response r = snapshot.data;
                      Map<String, dynamic> dados = json.decode(r.body);
                      String nome = "";
                      String gravidade = "";
                      String populacao = "";

                      if (dados.containsKey("details") &&
                          dados["details"] == "NOT FOUND") {
                        nome = "PLANETA NÃO ENCONTRADO";
                      } else {
                        nome = dados["name"];
                        gravidade = dados["gravity"];
                        populacao = dados["population"];
                      }
                      return Column(
                        children: [
                          Padding(padding: EdgeInsets.all(10)),
                          Text(
                            "Nome: " + nome,
                            style: TextStyle(fontSize: 17),
                          ),
                          Padding(padding: EdgeInsets.all(3)),
                          Text("Gravidade: " + gravidade),
                          Padding(padding: EdgeInsets.all(3)),
                          Text("População: " + populacao)
                        ],
                      );
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
