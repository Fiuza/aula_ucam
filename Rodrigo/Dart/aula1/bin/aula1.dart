import 'package:aula1/aula1.dart' as aula1;
import 'package:aula1/classe.dart';

void main(List<String> arguments) {
  var p1 = Pessoas();
  var p2 = Pessoas();

  p1.Pessoa1('M', 15);
  p1.Escrever();
  p2.Pessoa2(nome: 'TheOne', idade: 50, sexo: 'F');
  p2.Escrever();

  var a1 = Aula();
  var a2 = Aula();

  a1.Aula1('Quarta-feira');
  a1.Escrever();
  a2.Aula2(nome: 'Matemática', dia: 'Sexta-feira');
  a2.Escrever();
}
