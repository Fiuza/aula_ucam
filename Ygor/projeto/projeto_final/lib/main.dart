import 'package:flutter/material.dart';
import 'package:projeto_final/produtos_categoria.dart';
import 'package:woocommerce/woocommerce.dart';

import 'custom_widget/cardProdutos.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Xtreme Sport Shop',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(title: 'Xtreme Sport Shop'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  WooCommerce wooCommerce = WooCommerce(
    baseUrl: "https://aula.dreduardoterra.com.br/index.php/",
    consumerKey: "ck_e0f215cef0be5df4361e8cf6fd01f369ec10bcf3",
    consumerSecret: "cs_569bdc3deb493e4d40bac2f46c6767bdd6970bac",
    isDebug: true,
  );
  Future<List<WooProductCategory>> categorias;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FutureBuilder(
              future: wooCommerce.getProducts(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  List<WooProduct> produtos = snapshot.data as List<WooProduct>;
                  List<Widget> widgetProdutos = [];
                  if (produtos != null) {
                    produtos.forEach((produto) {
                      widgetProdutos.add(CardProdutos(produto));
                    });
                  }
                  return GridView.count(
                    shrinkWrap: true,
                    crossAxisCount: 2,
                    children: widgetProdutos,
                  );
                } else {
                  print(snapshot.error);
                  return Text("error");
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
