import 'package:flutter/material.dart';
import 'package:woocommerce/woocommerce.dart';

class CardProdutos extends StatelessWidget {
  final WooProduct produto;

  CardProdutos(this.produto);

  @override
  Widget build(BuildContext context) {
    String noPic =
        "https://st4.depositphotos.com/14953852/24787/v/600/depositphotos_247872612-stock-illustration-no-image-available-icon-vector.jpg";

    String srcImage =
        (this.produto.images.length > 0) ? this.produto.images[0].src : noPic;
    return Card(
        elevation: 10,
        child: SizedBox(
            child: Column(
          children: [
            Image.network(srcImage, fit: BoxFit.scaleDown),
            Expanded(
                child: ListTile(
              title: Text(
                this.produto.name,
                style: TextStyle(fontSize: 13),
              ),
              //subtitle: Text(this.produto.description),
              //isThreeLine: true,
              trailing: Text(this.produto.price),
            ))
          ],
        )));
  }
}
