	ListIsso, é uma lista de tarefa simples para dar conta de todas as atividades que temos no nosso dia dia. 
	Durante nosso dia as vezes vamos fazendo as coisas sem ao menos pensar no que iremos fazer depois e acabamos perdendo tempo pensando qual será nosso próximo passo.
	Muitas vezes devemos nos programar para o dia seguinte, para realizar uma tarefa e ter um dia produtivo, ver quais tarefas foram realizados naquele dia.
	Antes do avanço da tecnologia, era utilizado papel e caneta para fazer listas de afazeres e de compras, sejam ela de supermecados, farmácia, feiras 
	entre outros.
	Como muitas coisas passaram a ser digitais, por que não ter uma lista de tarefa no seu celular? Onde não há utilização de papel(diminui o uso do papel para esse fim)
	não há a utilização de canetas para escrever, nem ter que carrega-las para quando finalizar a tarefa ou pegar algum item risca-la, com o ListIsso poderá ser feito no
	seu celuar comm um aplicativo leve, moderno, minimalista, e fácil manuseio.
	No ListIsso ao voce adicionar uma tarefa "New Task", ela ficará salva ate que voce exclua independente se o app estiver aberto ou fechado, ela irá ter um simbolo de !
	para as tarefas não concluidas, ao concluir a mesma ela mudará o icone para um "check", e ao atualizar o app as tarefas concluidas será movida para baixo e as que
	nao estiverem concluidas serão movidas para cima.
	Também e possivel excluir a tarefa arrastando para o lado. Ao exclui irá aparecer uma SnackBar onde será possivel reverter o item excluido, retornando-o para o seu
	local de origem.
	ListIsso é fácil, simples, interativo e o melhor se encontra no que voce sempre leva com voce! Seu celular, esqueça os papeis e canetas! ListIsso! 


