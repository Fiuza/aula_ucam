const axios = require('axios').default;

/* HTTP
O HTTP é o protocolo responsável pela comunicação de sites na web. Quando acessamos um site,
 é esse protocolo que utilizamos. Esse protocolo possui alguns métodos, ou, como também são chamados, verbos.
*/

/* GET
As requisições do tipo GET são recomendadas para obter dados de um determinado recurso.
Como em um formulário de busca ou em uma listagem de todos os produtos cadastrados.
*/

/* POST
Já as requisições POST são mais utilizadas para para enviar
 informações para serem processadas, como por exemplo, criar algum recurso, como um produto, ou um cliente.
*/

/* ------------------------------ */

// exercicio GET: Retorna se o site e verídico ou não.

/*axios.get('https://www.google.com').then((response)=> {
    console.log(response.status);
});*/

//exercicio 2: Irá pegar a parte head do html.

/*axios.get('https://www.twitch.tv').then((response)=> {
    console.log(response.headers.data);
    console.log(response.data);
}).catch(error => {
    console.log(error);
  });*/


// exercicio POST: 

/*axios.post('http://localhost:3000/nome',{ // envia um dado para o servidor, neste caso o nome "ygor" 
    'nome':'Ygor' 
}).then((res)=>{    //ele ira responder o que o servidor falou
    console.log(res.data)
}); */

//exercicio 2:






