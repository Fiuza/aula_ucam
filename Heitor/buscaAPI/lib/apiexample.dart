import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'dart:convert';

class APIExample extends StatefulWidget {
  @override
  _APIExampleState createState() => _APIExampleState();
}

class _APIExampleState extends State<APIExample> {
  var _formKey = GlobalKey<FormState>();
  int numero;
  Future _personagem;
  Future _planeta;

  _search() {
    _formKey.currentState.save();
    setState(() {
      String url = "https://swapi.dev/api/";
      _personagem = http.get(url + "people/$numero/");
      
       String url2 = "https://swapi.dev/api/";
      _planeta = http.get(url2+"planets/$numero/");
    });
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Busca'),
      ),
      floatingActionButton: IconButton(
          icon: Icon(
            Icons.search,
            color: Colors.blue,
          ),
          onPressed: () => _search()),
      body: Container(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                keyboardType: TextInputType.number,
                onSaved: (numeroToSearch) => numero = int.parse(numeroToSearch),
                decoration: InputDecoration(labelText: "Digite um número para efetuar a busca"),
              ),
              
              FutureBuilder(
                future: _personagem,
                builder: (context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                    case ConnectionState.waiting:
                      return Text("Aguardando resposta");
                    default:
                      Response r = snapshot.data;
                      Map<String, dynamic> dados = json.decode(r.body);
                      String nome = "";
                      String sexo = "";
                      String altura = "";
                      String peso = "";
                      String corP = "";
                      String corO = "";
                      if (dados.containsKey("details") &&
                          dados["details"] == "NOT FOUND") {
                        nome = "NÃO ENCONTRADO";
                        sexo = "NÃO ENCONTRADO";
                        altura = "NÃO ENCONTRADO";
                        peso = "NÃO ENCONTRADO";
                        corP = "NÃO ENCONTRADO";
                        corO = "NÃO ENCONTRADO";
                      } else {
                        nome = dados["name"];
                        sexo = dados["gender"];
                        altura = dados["height"];
                        peso = dados["mass"];
                        corP = dados["skin_color"];
                        corO = dados["eye_color"];
                      }
                      return Column(
                        children: [
                          Text("Nome " + nome),
                          Text("Sexo " + dados["gender"] ?? ''),
                          Text("Altura" + altura),
                          Text("Peso:" + peso),
                          Text("Cor da pele:" + corP),
                          Text("Cor dos olhos:" + corO)
                        ],
                      );
                  }
                },
              ),

          

              FutureBuilder(
                      future: _planeta,
                      builder: (context, snapshot){
                        switch (snapshot.connectionState){
                          case ConnectionState.none:
                          case ConnectionState.waiting:
                            return Text("Aguardando resposta");
                          default:
                            Response r = snapshot.data;
                            Map<String,dynamic> dados = json.decode(r.body);
                            String nome = "";
                            String rotacao = "";
                            String terreno = "";
                            String diametro = "";
                            String clima  = "";
                            String populacao = "";
                            if (dados.containsKey("details") &&
                             dados["details"]== "NOT FOUND" ) {
                              nome = "NÃO ENCONTRADO";
                              rotacao = "NÃO ENCONTRADO";
                              terreno = "NÃO ENCONTRADO";
                              diametro = "NÃO ENCONTRADO";
                              clima = "NÃO ENCONTRADO";
                              populacao = "NÃO ENCONTRADO";
                            }else{
                              nome = dados ["name"];
                              rotacao = dados ["rotation_period"];
                              terreno = dados ["terrain"];
                              diametro = dados ["diameter"];
                              clima = dados ["climate"];
                              populacao = dados ["population"];
                            }
                            return Column(
                              children: [
                               Text("Nome:" + nome),
                               Text("Periodo de rotação:" + rotacao),
                               Text("Terreno:" + terreno),
                               Text("Diametro:" + diametro),
                               Text("Clima:" + clima),
                               Text("População:" + populacao)
                              ],
                            );
                     }
                  },
               ),
            ],
          ),
        ),
      ),
    );
  }
}



