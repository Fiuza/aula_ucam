import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'apiexample.dart';

SharedPreferences prefs;
main() async {
  WidgetsFlutterBinding.ensureInitialized();
  prefs = await SharedPreferences.getInstance();
  print('aguardou');
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Busca',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: APIExample(),
    );
  }
}


      
