// ignore_for_file: deprecated_member_use

import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
void main(List<String> args) {
  runApp(MaterialApp(
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  const Home({ Key key }) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  final _toDoController = TextEditingController();

  List _toDoList = [];
  Map<String, dynamic> _lastRemoved;
  int _lastRemovedPosition;

  get snack => null;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    
    _readData().then((data) {
      setState(() {
        _toDoList = json.decode(data);
      });
      
    });
  }


  void _addToDo() {
      setState(() {
        Map<String, dynamic> newToDo = Map();
        newToDo["title"] = _toDoController.text;
        _toDoController.text = "";
        newToDo["ok"] = false;
        _toDoList.add(newToDo);
        _saveData();
      });
  }

  Future<Null> _refresh() async {
    await Future.delayed(Duration(seconds: 1));

      setState(() {
        _toDoList.sort((x,y){
          if(x["ok"] && !y["ok"]) return 1;
          else if (!x["ok"] && y["ok"]) return -1;
          else return 0;
      });
    
    });

    _saveData();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ListIsso 📋"),
        backgroundColor: Colors.purple[900],
        centerTitle: true,
      ),
      body: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(17.0, 1.0, 7.0, 1.0),
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: _toDoController,
                  decoration: InputDecoration(
                    labelText: "New Task",
                    labelStyle: TextStyle(color: Colors.purple[600])
                  ),
                )
                  ),
                  RaisedButton(
                    color: Colors.purple[600],
                    child: Text("Add"),
                    textColor: Colors.white,
                    onPressed: _addToDo,

                    )
              ],
            ),
          ),
          Expanded(
            child: RefreshIndicator(onRefresh: _refresh,
              child: 
              ListView.builder(
              padding: EdgeInsets.only(top: 10.0),
              itemCount: _toDoList.length,
              itemBuilder: biuldItem),
            )
            ),
      ],) ,
    );
  }
//Ele faz cada um dos itens:
  Widget biuldItem(BuildContext context, int index) {
    return Dismissible(
      //utilizado para passa um nome qualquer, que ira pegar a hora atual e tranformar e string
      key: Key(DateTime.now().millisecondsSinceEpoch.toString()),
      background: Container(
        color: Colors.red[900],
        child: Align(
           alignment: Alignment(-0.9,0.0),
           child: Icon(Icons.delete, color: Colors.white),
           ),
      ),
      direction: DismissDirection.startToEnd,
      child: CheckboxListTile(
                  title: Text(_toDoList[index] ["title"]),
                  value: _toDoList[index] ["ok"],
                  checkColor: Colors.white,
                  activeColor:Colors.purple,
                  secondary: CircleAvatar(
                    child: Icon(_toDoList[index] ["ok"] ? 
                      Icons.check : Icons.priority_high), backgroundColor: Colors.purple[800], ),
                  onChanged: (c) {
                    setState(() {
                      _toDoList[index] ["ok"] = c;
                      _saveData();
                    });
                  },

                ),
     onDismissed: (direction) {
       setState(() {
         _lastRemoved = Map.from(_toDoList[index]);
         _lastRemovedPosition = index;
         _toDoList.removeAt(index);

       _saveData();

        final snack = SnackBar(
          content: Text("Tarefa ${_lastRemoved["title"]} deletada."),
          action: SnackBarAction(label: "Desfazer", 
          onPressed: () {
            setState(() {
              _toDoList.insert(_lastRemovedPosition, _lastRemoved);
              _saveData();
            });
          }
          ),
          duration: Duration(seconds: 2),
        );

          Scaffold.of(context).showSnackBar(snack);

       });

     }           
    );
  }
  


//Obter o arquivo:
Future<File> _getFile() async {
  final directory = await getApplicationDocumentsDirectory();
  return File("${directory.path}/list.json");
}
//Salvar algum dado no arquivo
Future<File> _saveData() async {
  String data = json.encode(_toDoList);
  final file = await _getFile();
  return file.writeAsString(data);
}
//Parta ler os dados do arquivo:
Future<String> _readData() async {
  try{
    final file = await _getFile();
    return file.readAsString();
  }catch (e) {
    return null;
  }
}
}






