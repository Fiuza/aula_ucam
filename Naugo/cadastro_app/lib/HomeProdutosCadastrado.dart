import 'package:cadastro_app/FormularioDeCadastroDeProdutos.dart';
import 'package:flutter/cupertino.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'HomePessoasCadastradas.dart';
import 'TelaDeLogin.dart';

class HomeProdutoCadastro extends StatelessWidget {
  final FirebaseFirestore firestore = FirebaseFirestore.instance;
  final FirebaseAuth auth = FirebaseAuth.instance;
  final GlobalKey<FormState> _form= new GlobalKey<FormState>();


  _checkHome(context)
  {
    auth.authStateChanges().listen((user) {
      if(user == null)
      {
        Navigator.of(context).push(MaterialPageRoute(builder:(context) => Login()));
      }
    });
  }

  _logout()
  {
    auth.signOut();
  }

  @override
  Widget build(BuildContext context) {
    _checkHome(context);
    return Scaffold(
      appBar: AppBar(
        key: _form,
        backgroundColor: Colors.red,
        title: Text("Produtos Cadastrado"),
      actions: [
        IconButton(
          icon: Icon(Icons.add),
          onPressed: () => {Navigator.of(context).push(MaterialPageRoute(builder: (context) =>  CadastroDeProdutos()))},
        ),

      ],
      ),
      floatingActionButton: IconButton(onPressed:() => _logout(), icon: Icon(Icons.logout)),
      body: Container(

          child: StreamBuilder(
            stream: firestore.collection("produtos").orderBy("nomedoproduto").snapshots(),
            builder: (context, RetornoDeDados)
            {
              switch (RetornoDeDados.connectionState)
              {
                case ConnectionState.none:
                case ConnectionState.waiting:
                  return Container(child: Text("Carregando..."),);
                default:
                  QuerySnapshot<Map<String, dynamic>> dados =
                  RetornoDeDados.data as QuerySnapshot<Map<String, dynamic>>;
                  List<Widget> listaProdutos =[];
                  dados.docs.forEach((element) {
                    Map<String, dynamic> infoProdutos = element.data();

                    listaProdutos.add( ListTile (leading: Icon(Icons.content_paste),title: Text(infoProdutos["nomedoproduto"])));
                    listaProdutos.add( ListTile (leading: Icon(Icons.shopping_cart),title: Text(infoProdutos["quantidade"])));
                  });
                  return (RetornoDeDados.hasData)? Column( children: listaProdutos ) : Text("AINDA NÃO TEM NENHUM Produto CADASTRADO");
              }
            },
          )
      ),
    );
  }
}
