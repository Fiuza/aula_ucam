import 'package:cadastro_app/HomeProdutosCadastrado.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'HomePessoasCadastradas.dart';
import 'TelaDeLogin.dart';

class Escolher extends StatelessWidget {

  final FirebaseFirestore firestore = FirebaseFirestore.instance;
  final FirebaseAuth auth = FirebaseAuth.instance;

  _checkHome(context)
  {
    auth.authStateChanges().listen((user) {
      if(user == null)
      {
        Navigator.of(context).push(MaterialPageRoute(builder:(context) => Login()));
      }
    });
  }

  _logout()
  {
    auth.signOut();
  }

  @override
  Widget build(BuildContext context) {
    _checkHome(context);
    return Scaffold(
      appBar: AppBar(),
      floatingActionButton: IconButton(onPressed:() => _logout(), icon: Icon(Icons.logout)),
      body: Padding(
        padding: const EdgeInsets.all(30),
        child: Form(
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Text(
                    'Escolha',
                    style: TextStyle(
                        fontWeight: FontWeight.w900,
                        fontSize: 60.0,
                        color: Colors.blue),
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  Container(
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    width: double.infinity,
                    height: 60,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                    ),
                    child: MaterialButton(
                      color: Colors.blue,
                      onPressed: () => {Navigator.of(context).push(MaterialPageRoute(builder: (context) =>  HomePessoasCadastradas()))},
                      child: Text(
                        'Pessoas cadastradas ',
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),

                  Container(
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    width: double.infinity,
                    height: 60,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                    ),
                    child: MaterialButton(
                      onPressed: () => {Navigator.of(context).push(MaterialPageRoute(builder: (context) =>  HomeProdutoCadastro()))},
                      color: Colors.blue,
                      child: Text(
                        'Produtos Cadastrados',
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Divider(
                    color: Colors.black,
                    height: 30,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
