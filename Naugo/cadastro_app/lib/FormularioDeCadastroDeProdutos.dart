import 'package:cadastro_app/HomeProdutosCadastrado.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'HomePessoasCadastradas.dart';

class CadastroDeProdutos extends StatelessWidget {
  late String quantidade;
  late String nomedoproduto;
  final FirebaseFirestore firestore = FirebaseFirestore.instance;
  final GlobalKey<FormState> _form= new GlobalKey<FormState>();

  _saveForm()
  {
    _form.currentState!.save();
    Map<String, dynamic> produto =
    {
      "nomedoproduto": nomedoproduto,
      "quantidade": quantidade,
    };
    firestore.collection("produtos").add(produto).then((produtossalvo) => {

    });

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.all(30),
        child: Form(
          key: _form,
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Text(
                    'Cadastro de Produtos',
                    style: TextStyle(
                        fontWeight: FontWeight.w900,
                        fontSize: 60.0,
                        color: Colors.blue),
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  TextFormField(
                    onSaved: (value) => nomedoproduto = value!,
                    decoration: InputDecoration(
                      labelText: 'Nome do Produto',
                      border: OutlineInputBorder(),
                      prefixIcon: Icon(Icons.content_paste),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    onSaved: (value) => quantidade = value!,
                    decoration: InputDecoration(
                      labelText: 'Quantidade',
                      border: OutlineInputBorder(),
                      prefixIcon: Icon(Icons.shopping_cart),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    width: double.infinity,
                    height: 60,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                    ),
                    child: MaterialButton(
                      onPressed: () => _saveForm() & {Navigator.of(context).push(MaterialPageRoute(builder: (context) =>  HomeProdutoCadastro()))},
                      color: Colors.blue,
                      child: Text(
                        'CADASTRAR',
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Divider(
                    color: Colors.black,
                    height: 30,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
