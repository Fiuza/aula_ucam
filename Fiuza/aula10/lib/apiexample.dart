import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'dart:convert';

class APIExample extends StatefulWidget {
  @override
  _APIExampleState createState() => _APIExampleState();
}

class _APIExampleState extends State<APIExample> {
  var _formKey = GlobalKey<FormState>();
  int numero;
  Future _personagem;
  Future _planeta;

  var _scafoldKey = GlobalKey<ScaffoldState>();

  String nome;

  _search() {
    _formKey.currentState.save();
    setState(() {
      String url = "https://swapi.dev/api/";
      _personagem = http.get(url + "people/$numero/");
    });
  }

  _save() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      var dados = {"numero": numero.toString(), "nome": nome};

      Response r = await http.post('http://192.168.0.9:3000/', body: dados);
      showDialog(
        context: _scafoldKey.currentContext,
        builder: (context) => AlertDialog(
          title: Text("Enviado com sucesso"),
          content: Text("Salvo no banco de dados"),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scafoldKey,
      appBar: AppBar(
        title: Text('API'),
      ),
      floatingActionButton: IconButton(
          icon: Icon(
            Icons.save,
            size: 50,
            color: Colors.blue,
          ),
          onPressed: () => _save()),
      body: Container(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                keyboardType: TextInputType.text,
                onSaved: (value) => nome = value,
                decoration: InputDecoration(labelText: "Digite o nome"),
              ),
              TextFormField(
                keyboardType: TextInputType.number,
                onSaved: (numeroToSearch) => numero = int.parse(numeroToSearch),
                decoration: InputDecoration(labelText: "Digite a idade"),
              ),
              FutureBuilder(
                future: _personagem,
                builder: (context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                    case ConnectionState.waiting:
                      return Text("Aguardando resposta");
                    default:
                      Response r = snapshot.data;
                      Map<String, dynamic> dados = json.decode(r.body);
                      String nome = "";
                      String sexo = "";
                      if (dados.containsKey("details") &&
                          dados["details"] == "NOT FOUND") {
                        nome = "NÃO ENCONTRADO";
                      } else {
                        nome = dados["name"];
                        sexo = dados["gender"];
                      }
                      return Column(
                        children: [
                          Text("Nome " + nome),
                          Text("Sexo " + dados["gender"] ?? '')
                        ],
                      );
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
