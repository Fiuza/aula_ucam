import 'package:flutter/material.dart';

class Navegacao extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Navegacao"),
      ),
      body: SingleChildScrollView(
        child: Text("Teste"),
      ),
      floatingActionButton: FlatButton(
        child: Text("Teste"),
        onPressed: () => Navigator.of(context).pop(),
      ),
    );
  }
}
