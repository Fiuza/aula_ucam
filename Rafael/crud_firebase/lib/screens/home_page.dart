import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:crud_firebase/models/aluno_model.dart';
import 'package:crud_firebase/repositories/aluno_repositories.dart';
import 'package:crud_firebase/repositories/alunos.dart';
import 'package:crud_firebase/screens/aluno_add_modal.dart';
import 'package:crud_firebase/services/auth_services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Aluno> alunosSelecionados = [];

  appBarDinamica() {
    if (alunosSelecionados.isEmpty) {
      return AppBar(
        title: Text('Cadastro'),
        centerTitle: true,
      );
    } else {
      return AppBar(
        title: Text('${alunosSelecionados.length} selecionados'),
        actions: [
          IconButton(
              onPressed: () {
                setState(() {
                  alunosSelecionados.clear();
                });
              },
              icon: Icon(Icons.cleaning_services)),
        ],
        centerTitle: true,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    // final Stream<QuerySnapshot> _usersStream = FirebaseFirestore.instance
    //     .collection('alunos')
    //     .orderBy('matricula')
    //     .snapshots();

    CollectionReference alunosCollection =
        FirebaseFirestore.instance.collection('alunos');

    return Scaffold(
      appBar: appBarDinamica(),
      drawer: Drawer(
        child: ListView(
          children: [
            UserAccountsDrawerHeader(
              accountName: Text(
                'Nome do Usuário',
                style: TextStyle(fontSize: 18),
              ),
              accountEmail: Text('exemplo@email.com',
                  style: TextStyle(color: Colors.white60)),
              currentAccountPicture: Icon(
                Icons.account_circle,
                size: 50,
                color: Colors.white,
              ),
              decoration: BoxDecoration(
                color: Colors.indigo,
              ),
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text(
                'Sair',
                style: TextStyle(fontSize: 16),
              ),
              tileColor: Colors.grey[200],
              onTap: () => context.read<AuthService>().logout(),
            ),
          ],
        ),
      ),
      //backgroundColor: Colors.grey.shade300,
      body: StreamBuilder<QuerySnapshot>(
        stream: alunosCollection.orderBy('matricula').snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Algo deu errado');
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          }
          if (snapshot.data!.docs.length == 0) {
            return Center(
              child: Text('Nenhum aluno cadastrado'),
            );
          }
          return ListView(
            children: snapshot.data!.docs.map((alunosCollection) {
              Map<String, dynamic> data =
                  alunosCollection.data()! as Map<String, dynamic>;
              return Container(
                decoration: BoxDecoration(
                    //color: Colors.white,
                    borderRadius: BorderRadius.circular(8)),
                margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Slidable(
                  actionPane: SlidableDrawerActionPane(),
                  child: ListTile(
                    onTap: () {
                      //Page Aluno Update =============>
                      print('ListTile foi apertado');
                    },
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                    ),
                    leading: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      child: Icon(
                        Icons.person_outline,
                        size: 35,
                        color: Colors.indigo,
                      ),
                    ),
                    title: Text(data['nome']),
                    subtitle: Text(data['curso']),
                    trailing: Text(data['matricula'].toString()),
                  ),
                  secondaryActions: [
                    IconSlideAction(
                      caption: 'Delete',
                      color: Colors.red,
                      icon: Icons.delete,
                      onTap: () {
                        alunosCollection.reference.delete().then(
                              (value) =>
                                  ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  content: Text('Deletado com suceso'),
                                  backgroundColor: Colors.green[800],
                                ),
                              ),
                            );
                      },
                    ),
                  ],
                ),
              );
            }).toList(),
          );
        },
      ),

      /// Inicio floatingActionButton adicionar =============>
      floatingActionButton: (alunosSelecionados.isEmpty)
          ? FloatingActionButton(
              onPressed: () {
                showBarModalBottomSheet(
                  context: context,
                  builder: (context) => AlunoModalAdd(),
                );
              },
              child: Icon(Icons.add),
            )

          /// Inicio floatingActionButton deletar =============>
          : FloatingActionButton(
              onPressed: () {},
              backgroundColor: Colors.red,
              child: Icon(Icons.delete),
            ),
    );
  }
}
