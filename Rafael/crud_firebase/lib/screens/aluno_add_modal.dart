import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:crud_firebase/models/aluno_model.dart';
import 'package:crud_firebase/screens/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:validatorless/validatorless.dart';

class AlunoModalAdd extends StatefulWidget {
  const AlunoModalAdd({Key? key}) : super(key: key);

  @override
  _AlunoModalAddState createState() => _AlunoModalAddState();
}

class _AlunoModalAddState extends State<AlunoModalAdd> {
  TextEditingController nomeAluno = TextEditingController();
  TextEditingController matriculaAluno = TextEditingController();
  TextEditingController cursoAluno = TextEditingController();
  final formKey = GlobalKey<FormState>();
  final firestoreInstance = FirebaseFirestore.instance;

  mensagem(String msg, BuildContext context) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(msg),
        backgroundColor: Colors.green[800],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      height: MediaQuery.of(context).size.height * 0.9,
      child: Padding(
        padding: EdgeInsets.all(15),
        child: Form(
          key: formKey,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 10, bottom: 10),
                  child: Text(
                    'Adicionar Aluno',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
                TextFormField(
                  controller: nomeAluno,
                  decoration: InputDecoration(
                    labelText: 'Nome do Aluno',
                  ),
                  validator: Validatorless.multiple([
                    Validatorless.required('Nome do aluno Obrigatório'),
                  ]),
                ),
                TextFormField(
                  controller: matriculaAluno,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    labelText: 'Matrícula',
                  ),
                  validator: Validatorless.multiple([
                    Validatorless.number('Sómente números'),
                    Validatorless.required('Matríula Obrigatória'),
                  ]),
                ),
                TextFormField(
                  controller: cursoAluno,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    labelText: 'Curso',
                  ),
                  validator: Validatorless.multiple(
                      [Validatorless.required('Campo Obrigatório')]),
                ),

                /// ==========> Botão salvar
                Padding(
                  padding: const EdgeInsets.only(top: 50),
                  child: MaterialButton(
                    onPressed: () {
                      if (formKey.currentState!.validate()) {
                        if (addAluno()) {
                          mensagem('Aluno adicionado com sucesso', context);
                        } else {
                          mensagem(
                              'Não foi possível adicional o aluno', context);
                        }
                        Navigator.pop(context);
                      }
                    },
                    minWidth: double.infinity,
                    height: 55,
                    color: Colors.indigo,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      'Salvar',
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  addAluno() {
    firestoreInstance.collection('alunos').add({
      'nome': nomeAluno.text,
      'matricula': matriculaAluno.text,
      'curso': cursoAluno.text,
    });
    Navigator.pop(context);
  }

  limparTela() {
    nomeAluno.clear();
    matriculaAluno.clear();
    cursoAluno.clear();
  }
}
