import 'package:crud_firebase/screens/criar_conta_page.dart';
import 'package:crud_firebase/screens/home_page.dart';
import 'package:crud_firebase/services/auth_services.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:validatorless/validatorless.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  late bool _showPassword = false;
  final email = TextEditingController();
  final senha = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  bool loading = false; //carregar o loading

  // @override
  // void dispose() {
  //   // TODO: implement dispose
  //   email.dispose();
  //   senha.dispose();

  //   super.dispose();
  // }

  // @override
  // void initState() {
  //   // TODO: implement initState
  //   super.initState();

  //   email.addListener(() => setState(() {}));
  // }

  ///método login
  login() async {
    setState(() => loading = true);
    try {
      // await Provider.of<AuthService>(context)
      //     .login(email.text, senha.text);
      await context.read<AuthService>().login(email.text, senha.text);
    } on AuthException catch (e) {
      setState(() => loading = false);
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(e.mensagem)));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      ///---------->
      body: SingleChildScrollView(
        ///---------->
        child: Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.symmetric(horizontal: 30, vertical: 50),

          ///---------->
          child: Column(
            children: <Widget>[
              Container(
                child: Icon(
                  Icons.account_circle,
                  color: Colors.indigo[300],
                  size: 70,
                ),
              ),

              ///---------->
              Column(
                children: <Widget>[
                  Text(
                    'Login',
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              SizedBox(
                height: 40,
              ),

              ///----------> container dos TextInputs
              Container(
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      TextFormField(
                        controller: email,
                        validator: Validatorless.multiple([
                          Validatorless.required('E-mail obrigatório'),
                          Validatorless.email('E-mail inválido')
                        ]),
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          labelText: 'E-mail',
                          prefixIcon: Icon(Icons.email_outlined),
                          suffixIcon: email.text.isEmpty
                              ? Container(width: 0)
                              : IconButton(
                                  icon: Icon(Icons.close),
                                  onPressed: () => email.clear(),
                                ),
                          border: OutlineInputBorder(),
                          // errorText: 'E-mail inválido',
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 18),
                        child: TextFormField(
                          controller: senha,
                          keyboardType: TextInputType.number,
                          obscureText: _showPassword == false ? true : false,
                          validator: Validatorless.multiple([
                            Validatorless.required('Senha Obrigatória'),
                            Validatorless.min(
                                6, 'Sua senha deve ter no mínimo 6 caracteres'),
                          ]),
                          decoration: InputDecoration(
                            labelText: 'Senha',
                            prefixIcon: Icon(Icons.lock_outline),
                            suffixIcon: IconButton(
                              icon: Icon(
                                _showPassword == false
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                              ),
                              onPressed: () {
                                setState(() {
                                  _showPassword = !_showPassword;
                                });
                              },
                            ),
                            border: OutlineInputBorder(),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),

              ///----------> container dos botões
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 25),
                child: Container(
                  child: Column(
                    children: <Widget>[
                      //---------------> botão de login
                      MaterialButton(
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            login();
                          }
                        },
                        minWidth: double.infinity,
                        height: 50,
                        elevation: 2,
                        color: Colors.indigo,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: (loading)
                            ? Padding(
                                padding: EdgeInsets.all(16),
                                child: SizedBox(
                                  width: 24,
                                  height: 24,
                                  child: CircularProgressIndicator(
                                    color: Colors.white,
                                  ),
                                ),
                              )
                            : Text(
                                'Entrar',
                                style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 18,
                                    color: Colors.white),
                              ),
                      ),

                      //---------------> botão para cadastrar
                      Padding(
                        padding: const EdgeInsets.only(top: 15),
                        child: MaterialButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => CriarContaPage()));
                          },
                          minWidth: double.infinity,
                          height: 50,
                          elevation: 2,
                          shape: RoundedRectangleBorder(
                            side: BorderSide(
                              color: Colors.indigo,
                            ),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Text(
                            'Cadastrar',
                            style: TextStyle(
                              color: Color(0xff172126),
                              fontSize: 18,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
