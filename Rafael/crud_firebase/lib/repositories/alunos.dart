import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:crud_firebase/databases/db_firestore.dart';
import 'package:crud_firebase/models/aluno_model.dart';
import 'package:crud_firebase/repositories/aluno_repositories.dart';
import 'package:crud_firebase/services/auth_services.dart';
import 'package:flutter/cupertino.dart';

class AlunosRepository extends ChangeNotifier {
  List<Aluno> _listAlunos = [];
  late FirebaseFirestore db;

  ///Contrutor
  AlunosRepository() {
    _startRepository();
  }

  ///Inicia o repositório
  _startRepository() async {
    await _startFirestore();
    await readAlunos();
  }

  ///Recupera a instância do DBFirestore
  _startFirestore() {
    db = DBFirestore.get();
  }

  readAlunos() {
    Stream<QuerySnapshot> _usersStream = FirebaseFirestore.instance
        .collection('alunos')
        .orderBy('matricula')
        .snapshots();
    notifyListeners();
  }

  ///Buscar os alunos no DBFirestore
  // _readAlunos() async {
  //   if (_listAlunos.isEmpty) {
  //     final snapshot =
  //         await db.collection('alunos').snapshots();
  //     snapshot.forEach((doc) {
  //       Aluno aluno = AlunoRepositories.lista_aluno
  //           .firstWhere((aluno) => aluno.matricula == doc.get('matricula'));
  //       _listAlunos.add(aluno);
  //       notifyListeners();
  //     });
  //   }
  // }

  ///
  // saveAll(List<Aluno> alunos) {
  //   alunos.forEach((aluno) async {
  //     if (!_listAlunos.any((atual) => atual.matricula == aluno.matricula)) {
  //       _listAlunos.add(aluno);
  //       //salva os dados no Firestore
  //       await db.collection('alunos').doc(aluno.matricula).set({
  //         'nome': aluno.nome,
  //         'matricula': aluno.matricula,
  //         'curso': aluno.curso,
  //         'foto': aluno.foto,
  //       });
  //     }
  //   });
  //   notifyListeners();
  // }

  update() {}

  ///Deletar o aluno
  // remove(Aluno aluno) async {
  //   await db.collection('alunos').doc(aluno.matricula).delete();
  //   _listAlunos.remove(aluno);
  //   notifyListeners();
  // }
}
