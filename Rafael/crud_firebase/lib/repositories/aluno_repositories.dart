import 'package:crud_firebase/models/aluno_model.dart';

class AlunoRepositories {
  static List<Aluno> lista_aluno = [
    Aluno(
      foto: 'https://avatars.githubusercontent.com/u/38574440?v=4',
      nome: 'Rafael',
      matricula: 001,
      curso: 'Engenharia Civil',
    ),
    Aluno(
      foto: 'https://avatars.githubusercontent.com/u/38574440?v=4',
      nome: 'Lucas',
      matricula: 002,
      curso: 'Engenharia Civil',
    ),
    Aluno(
      foto: 'https://avatars.githubusercontent.com/u/38574440?v=4',
      nome: 'Pedro',
      matricula: 003,
      curso: 'Engenharia Civil',
    ),
    Aluno(
      foto: 'https://avatars.githubusercontent.com/u/38574440?v=4',
      nome: 'João',
      matricula: 004,
      curso: 'Engenharia Civil',
    ),
  ];
}
