import 'package:crud_firebase/repositories/alunos.dart';
import 'package:crud_firebase/services/auth_services.dart';
import 'package:crud_firebase/theme/my_app.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => AuthService()),
        ChangeNotifierProvider(create: (context) => AlunosRepository())
      ],
      child: MyApp(),
    ),
  );
}
