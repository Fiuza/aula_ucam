class Aluno {
  late String foto;
  late String nome;
  late dynamic matricula;
  late String curso;

  Aluno({
    required this.foto,
    required this.nome,
    required this.matricula,
    required this.curso,
  });
}
