import 'package:cloud_firestore/cloud_firestore.dart';

///Classe para configurar o banco de dados Firestore
class DBFirestore {
  ///Contrutor privado
  DBFirestore._();

  //Instância do DBFirestore
  static final DBFirestore _instance = DBFirestore._();

  //Salvando a instância do Firestore
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  static FirebaseFirestore get() {
    return DBFirestore._instance._firestore;
  }
}
