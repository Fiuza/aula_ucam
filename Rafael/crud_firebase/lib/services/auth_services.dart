import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';

///Classe para implementar uma Exception padrão
class AuthException implements Exception {
  String mensagem;

  //Contrutor
  AuthException({required this.mensagem});
}

///Classe de autenticação do FirebaseAuth
class AuthService extends ChangeNotifier {
  //instânciando o FirebaseAuth
  FirebaseAuth _auth = FirebaseAuth.instance;

  User? usuario;
  bool isLoading = true;

  ///Contrutor da classe
  AuthService() {
    _authCheck();
  }

  ///Método para checar a autenticação no firebase
  _authCheck() {
    _auth.authStateChanges().listen((User? user) {
      //verifica se existe o usuário
      if (user == null) {
        usuario = null;
      } else {
        usuario = user;
      }
      //finalizar o loading
      isLoading = false;
      //notificar as Pages através do Provider
      notifyListeners();
    });
  }

  ///Método para recuperar as dados do usuário atual
  _getUser() {
    usuario = _auth.currentUser;
    notifyListeners();
  }

  ///Método de login no FirebaseAuth
  login(String email, String senha) async {
    try {
      await _auth.signInWithEmailAndPassword(email: email, password: senha);
      _getUser();
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        throw AuthException(
            mensagem: 'Nenhum usuário encontrado com esse e-mail.');
      } else if (e.code == 'wrong-password') {
        throw AuthException(mensagem: 'Senha inválida. Tente novamente.');
      }
    }
  }

  ///Método logout no FirebaseAuth
  logout() async {
    await _auth.signOut();
  }

  ///método registrar usuário no FirebaseAuth
  registrar(String email, String senha) async {
    try {
      await _auth.createUserWithEmailAndPassword(email: email, password: senha);
      _getUser();
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        throw AuthException(mensagem: 'A senha fornecida é muito fraca.');
      } else if (e.code == 'email-already-in-use') {
        throw AuthException(mensagem: 'A conta já existe para esse e-mail.');
      }
    }
  }
}
