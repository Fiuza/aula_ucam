import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
// ignore: unused_import
import 'package:firebase_core/firebase_core.dart';
import 'commons/blankPage.dart';

class HomePage extends StatelessWidget {
  final FirebaseFirestore firestore = FirebaseFirestore.instance;

  @override
  Widget build(BuildContext context) {
    return BlankPage(
        "Usuários cadastrados",
        Container(
            child: StreamBuilder(
          stream: firestore.collection("alunos").orderBy("nome").snapshots(),
          builder: (context, dadoRetornado) {
            switch (dadoRetornado.connectionState) {
              case ConnectionState.none:
              case ConnectionState.waiting:
                return Container(
                  child: Text("Aguarde..."),
                );
              default:
                QuerySnapshot<Map<String, dynamic>> dados =
                    dadoRetornado.data as QuerySnapshot<Map<String, dynamic>>;
                List<Widget> listaAlunos = [];
                dados.docs.forEach((element) {
                  Map<String, dynamic> infoAluno = element.data();
                  listaAlunos.add(ListTile(
                    title: Text(infoAluno["nome"]),
                  ));
                });
                return (dadoRetornado.hasData)
                    ? Column(
                        children: listaAlunos,
                      )
                    : Text("Nenhum aluno cadastrado");
            }
          },
        )));
  }
}
