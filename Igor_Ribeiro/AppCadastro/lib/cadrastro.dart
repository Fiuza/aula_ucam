import 'package:app_firestore/login.dart';
import 'package:flutter/material.dart';
// ignore: unused_import
import 'package:cloud_firestore/cloud_firestore.dart';
// ignore: unused_import
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
// ignore: unused_import
import 'package:flutter/services.dart';
// ignore: unused_import
import 'main.dart';

class CadastroPage extends StatefulWidget {
  CadastroPage({Key? key}) : super(key: key);

  @override
  _CadastroPageState createState() => _CadastroPageState();
}

class _CadastroPageState extends State<CadastroPage> {
  int matricula = 0;
  final FirebaseFirestore firestore = FirebaseFirestore.instance;
  final FirebaseAuth auth = FirebaseAuth.instance;
  late String nome;
  late String email;
  late String senha;
  final GlobalKey<FormState> _form = GlobalKey<FormState>();


  _saveForm() {
    _form.currentState!.save();
    Map<String, dynamic> aluno = {
      "nome": nome,
      "matricula": matricula,
      "email": email,
      "senha": senha,
      "created_at": DateTime.now()
    };

    auth
        .createUserWithEmailAndPassword(email: this.email, password: this.senha)
        .then((value) {
      String userUid = value.user!.uid;
      firestore.collection("alunos").doc(userUid).set(aluno);
    });
  }

  _checkLogin(Context) {
    auth.authStateChanges().listen((user) {
      if (user == null) {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => LoginPage()));
      }
    });
  }

  _logout() {
    auth.signOut();
  }

  @override
  Widget build(BuildContext context) {
    _checkLogin(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text("Cadastro"),
      ),
      floatingActionButton:
          IconButton(onPressed: () => _logout(), icon: Icon(Icons.logout)),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Form(
          key: _form,
          child: Column(
            children: [
              TextFormField(
                onSaved: (value) => nome = value!,
                decoration: InputDecoration(
                  labelText: "Nome",
                  helperText: "Obrigatório",
                  labelStyle: TextStyle(
                    color: Colors.black38,
                    fontWeight: FontWeight.w400,
                    fontSize: 20,
                  ),
                ),
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              TextFormField(
                onSaved: (value) => email = value!,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  labelText: "E-mail",
                  helperText: "Obrigatório",
                  labelStyle: TextStyle(
                    color: Colors.black38,
                    fontWeight: FontWeight.w400,
                    fontSize: 20,
                  ),
                ),
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              TextFormField(
                onSaved: (value) => senha = value!,
                obscureText: true,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  labelText: "Senha",
                  helperText: "Obrigatório",
                  labelStyle: TextStyle(
                    color: Colors.black38,
                    fontWeight: FontWeight.w400,
                    fontSize: 20,
                  ),
                ),
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
               SizedBox(
                height: 10,
              ),
              TextFormField(
                onSaved: (value) => matricula = int.parse(value!),
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  labelText: "Matrícula",
                  helperText: "Obrigatório",
                  labelStyle: TextStyle(
                    color: Colors.black38,
                    fontWeight: FontWeight.w400,
                    fontSize: 20,
                  ),
                ),
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                height: 60,
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    stops: [0.3, 1],
                    colors: [
                      Color(0xFFF44336),
                      Color(0xFFF92B7F),
                    ],
                  ),
                  borderRadius: BorderRadius.all(
                    Radius.circular(10),
                  ),
                ),
                child: SizedBox.expand(
                  // ignore: deprecated_member_use
                  child: FlatButton(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Salvar",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                            fontSize: 20,
                          ),
                          textAlign: TextAlign.left,
                        ),
                        Container(
                          child: SizedBox(
                            child: Image.asset('assets/images/image.png'),
                            height: 42,
                            width: 42,
                          ),
                        ),
                      ],
                    ),
                    onPressed: () =>  _saveForm(),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

