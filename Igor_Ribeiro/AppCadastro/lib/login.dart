import 'package:app_firestore/cadrastro.dart';
import 'package:app_firestore/home.dart';
import 'package:flutter/material.dart';
// ignore: unused_import
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
// ignore: unused_import
import 'main.dart';

// ignore: must_be_immutable
class LoginPage extends StatelessWidget {
  final GlobalKey<FormState> _form = GlobalKey<FormState>();
  final FirebaseAuth auth = FirebaseAuth.instance;
  late String email;
  late String senha;

  _authenticate(context) {
    _form.currentState!.save();
    auth
        .signInWithEmailAndPassword(email: this.email, password: this.senha)
        .then((value) => Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => HomePage()),
            ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(
          top: 90,
          left: 40,
          right: 40,
        ),
        color: Colors.white,
        child: Form(
          key: _form,
          child: Column(
            children: [
              SizedBox(
                width: 200,
                height: 200,
                child: Image.asset('assets/images/log2.png'),
              ),
              SizedBox(
                height: 15,
              ),
              TextFormField(
                onSaved: (value) => email = value!,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  labelText: "E-mail",
                  labelStyle: TextStyle(
                    color: Colors.black38,
                    fontWeight: FontWeight.w400,
                    fontSize: 20,
                  ),
                ),
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              TextFormField(
                onSaved: (value) => senha = value!,
                obscureText: true,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  labelText: "Senha",
                  labelStyle: TextStyle(
                    color: Colors.black38,
                    fontWeight: FontWeight.w400,
                    fontSize: 20,
                  ),
                ),
                style: TextStyle(fontSize: 20),
              ),
              SizedBox(
                height: 25,
              ),
              Container(
                height: 50,
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    stops: [0.3, 1],
                    colors: [
                      Color(0xFFF44336),
                      Color(0xFFF92B7F),
                    ],
                  ),
                  borderRadius: BorderRadius.all(
                    Radius.circular(10),
                  ),
                ),
                child: SizedBox.expand(
                  // ignore: deprecated_member_use
                  child: FlatButton(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Entrar",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                            fontSize: 20,
                          ),
                          textAlign: TextAlign.left,
                        ),
                        Container(
                          child: SizedBox(
                            child: Image.asset('assets/images/image.png'),
                            height: 42,
                            width: 42,
                          ),
                        ),
                      ],
                    ),
                    onPressed: () => _authenticate(context),
                  ),
                ),
              ),
               SizedBox(
                height: 25,
              ),
              
              Container(
                height: 50,
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    stops: [0.3, 1],
                    colors: [
                      Color(0xFFF44336),
                      Color(0xFFF92B7F),
                    ],
                  ),
                  borderRadius: BorderRadius.all(
                    Radius.circular(10),
                  ),
                ),
                child: SizedBox.expand(
                  // ignore: deprecated_member_use
                  child: FlatButton(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Cadastrar-se",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                            fontSize: 20,
                          ),
                          textAlign: TextAlign.left,
                        ),
                        Container(
                          child: SizedBox(
                            child: Image.asset('assets/images/image.png'),
                            height: 42,
                            width: 42,
                          ),
                        ),
                      ],
                    ),
                    //onPressed: () => _authenticate(context),
                    onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CadastroPage(),
                      ),
                    );
                  },

                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
