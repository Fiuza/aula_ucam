// ignore: unused_import
import 'package:app_firestore/commons/blankPage.dart';
// ignore: unused_import
import 'package:app_firestore/home.dart';
import 'package:app_firestore/login.dart';
// ignore: unused_import
import 'package:app_firestore/cadrastro.dart';
import 'package:flutter/material.dart';
// ignore: unused_import
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
// ignore: unused_import
import 'package:flutter/services.dart';

main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: LoginPage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int matricula = 0;
  final FirebaseFirestore firestore = FirebaseFirestore.instance;
  final FirebaseAuth auth = FirebaseAuth.instance;
  late String nome;
  late String email;
  late String senha;
  final GlobalKey<FormState> _form = GlobalKey<FormState>();

  @override
  initState() {
    super.initState();
  }

  // ignore: unused_element
  _saveForm() {
    _form.currentState!.save();
    Map<String, dynamic> aluno = {
      "nome": nome,
      "email": email,
      "senha": senha,
      "created_at": DateTime.now()
    };

    auth
        .createUserWithEmailAndPassword(email: this.email, password: this.senha)
        .then((value) {
      String userUid = value.user!.uid;
      firestore.collection("alunos").doc(userUid).set(aluno);
    });
  }

  _checkLogin(Context) {
    auth.authStateChanges().listen((user) {
      if (user == null) {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => LoginPage()));
      }
    });
  }

  // ignore: unused_element
  _logout() {
    auth.signOut();
  }

  @override
  Widget build(BuildContext context) {
    _checkLogin(context);
    return BlankPage("Home", Container());
  }
}

